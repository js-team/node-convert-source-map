# Installation
> `npm install --save @types/convert-source-map`

# Summary
This package contains type definitions for convert-source-map (https://github.com/thlorenz/convert-source-map).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/convert-source-map.

### Additional Details
 * Last updated: Mon, 06 Nov 2023 22:41:05 GMT
 * Dependencies: none

# Credits
These definitions were written by [Andrew Gaspar](https://github.com/AndrewGaspar), [Melvin Groenhoff](https://github.com/mgroenhoff), and [TeamworkGuy2](https://github.com/TeamworkGuy2).
